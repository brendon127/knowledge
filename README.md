# Tips, Tricks and Notes of technologies used 

The purpose of this repo is to serve as reference document to quickly find instructions on how to use the technologies included.
The benefit of this is to eliminate searching through other repositories and histories to find something or some way to solve a problem. 
It will also include notes of tutorials followed to find those steps/solutions quicker.
