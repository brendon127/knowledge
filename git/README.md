# GIT

### Jedi Mind Tricks Video
<a  target="_blank" href="https://www.youtube.com/watch?v=M75aENmuzmo">Video</a>
<br>
<a  target="_blank" href="https://www.slideshare.net/jankrag/jedi-mind-tricks-for-git">Slide Show</a>
<br>
### Git hooks
Commits have different phases. Hooks can be used to inject a step into the normal flow of git.

Getting started:
``` console
git init
ls .git/hooks
```
Two types of hooks:
* Server side 
* Client side

#### Client Hooks
##### pre-commit hook
* First thing after ```git commit```. (before commit message is specified)
* Able to abandon commit
* No arguments


Ensure Developers can't commit to master:
```  bash
#!/bin/bash
# Check where HEAD is at. (current branch)
if [ "$( git symbolic-ref --short HEAD)" == "master" ]; then
	echo "This is not the branch you are looking for"
	exit 1
fi
exit 0
```

##### commit-msg hook
Ensure that commit messages reference a valid issue number. If it does not, abandon the commit.

``` bash
#!/bin/bash
# $1 is the temp file containing the commit message
issue_number=`cat $1 | xargs| sed 's/.*#\([0-9]*\).*/\1/'`
repo="`git remote show origin | grep "Push" | xargs | sed  's/.*:\(.*\)/\1/' | sed 's/.git//'`" 

url=https://api.github.com/repos/$repo/issues/$issue_number
echo $url
curl -s $url | grep -q "\"Not Found\""

if [ $? -eq 0 ] 
then
	echo "The commit message contains an invalid issue number"
	exit 1
else
	echo "Succesful commit!"
	echo -n "Issue number: #"
	echo $issue_number
	exit 0
fi


```

